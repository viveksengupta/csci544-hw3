import hw3_corpus_tool
import sys

def create_advanced_features_file(ip_file):
	
	x = hw3_corpus_tool.get_utterances_from_filename(ip_file)
	
	numLines = len(x)
	#print(str(numLines))
	
	last_speaker = x[0].speaker
	op_string = ""
	
	for i in range(numLines):
		if x[i].act_tag != None:
			op_string += x[i].act_tag + "\t"
		#END if
		
		if i == 0:
			op_string += "FIRST_UTTERANCE\t"
		#END if
		if i == numLines - 1:
			op_string += "LAST_UTTERANCE\t"
		#END if
		
		curr_speaker = x[i].speaker
		if curr_speaker != last_speaker:
			op_string += "SPEAKER_CHANGED\t"
		#END if
		last_speaker = curr_speaker
		
		if x[i].pos != None:
			pos_size = len(x[i].pos)
			
			for j in range(pos_size):
				op_string += "TOKEN_" + x[i].pos[j].token + "\t"
				op_string += "POS_" + x[i].pos[j].pos + "\t"
				
				if j != 0:
					op_string += "TOKEN_BIGRAM_" + x[i].pos[j-1].token + "_" + x[i].pos[j].token + "\t"
					op_string += "POS_BIGRAM_" + x[i].pos[j-1].pos + "_" + x[i].pos[j].pos + "\t"
				#END if
				
				# Trigram code, commented out
				#if j != 0 and j != pos_size - 1:
					#op_string += "TOKEN_TRIGRAM_" + x[i].pos[j-1].token + "_" + x[i].pos[j].token + "_" + x[i].pos[j+1].token + "\t"
					#op_string += "POS_TRIGRAM_" + x[i].pos[j-1].pos + "_" + x[i].pos[j].pos + "_" + x[i].pos[j+1].pos + "\t"
				#END if
			#END for
		#END if
		
		op_string = op_string.strip()
		op_string += "\n"
		
	#END for
	
	op_string = op_string.rstrip("\n")
	sys.stdout.write(op_string)
#END create_advanced_features_file()


if __name__ == "__main__":
	create_advanced_features_file(sys.argv[1])
#END if