import hw3_corpus_tool
import sys

def create_baseline_features_file(ip_file):
	
	x = hw3_corpus_tool.get_utterances_from_filename(ip_file)
	
	numLines = len(x)
	#print(str(numLines))
	
	last_speaker = x[0].speaker
	op_string = ""
	
	for i in range(numLines):
		if x[i].act_tag != None:
			op_string += x[i].act_tag + "\t"
		#END if
		
		if i == 0:
			op_string += "FIRST_UTTERANCE\t"
		#END if
		
		curr_speaker = x[i].speaker
		if curr_speaker != last_speaker:
			op_string += "SPEAKER_CHANGED\t"
		#END if
		last_speaker = curr_speaker
		
		if x[i].pos != None:
			pos_size = len(x[i].pos)
			#op_file.write(str(pos_size) + " ")
			for j in range(pos_size):
				op_string += "TOKEN_" + x[i].pos[j].token + "\t"
				op_string += "POS_" + x[i].pos[j].pos + "\t"
			#END for
		#END if
		
		op_string = op_string.strip()
		op_string += "\n"
	#END for
	
	op_string = op_string.rstrip("\n")
	sys.stdout.write(op_string)
#END create_baseline_features_file()

if __name__ == '__main__':
	create_baseline_features_file(sys.argv[1])
#END if