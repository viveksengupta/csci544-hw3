import os
import sys

TRAIN_DIR = "data/train"
TEST_DIR = "data/test"
SELF_TRAIN_DIR = "data/self_train"
SELF_TEST_DIR = "data/self_test"

original_stdout = sys.stdout

baseline_train_file = open("swbdDAMSL.crfsuite.baseline.train","w")
baseline_test_file = open("swbdDAMSL.crfsuite.baseline.test","w")
advanced_train_file = open("swbdDAMSL.crfsuite.advanced.train","w")
advanced_test_file = open("swbdDAMSL.crfsuite.advanced.test","w")
baseline_self_train_file = open("crf.baseline.train","w")
baseline_self_test_file = open("crf.baseline.test","w")
advanced_self_train_file = open("crf.advanced.train.2","w")
advanced_self_test_file = open("crf.advaned.test.2","w")

for training_subdirs, training_dirs, training_files in os.walk(TRAIN_DIR):
	for training_file in sorted(training_files):
	
		training_file_path = os.path.join(TRAIN_DIR, training_file)
		print(training_file_path)
		
		# baseline training file
		baseline_python3_string = "python3 create_baseline_features.py " + training_file_path + " > temp_file"
		#print(baseline_python3_string)
		os.system(baseline_python3_string)
		
		baseline_cat_string = "cat temp_file >> swbdDAMSL.crfsuite.baseline.train"
		#print(baseline_cat_string)
		os.system(baseline_cat_string)
		
		baseline_echo_string = "echo \"\n\" >> swbdDAMSL.crfsuite.baseline.train"
		#print(baseline_echo_string)
		os.system(baseline_echo_string)
		
		# advanced test file
		advanced_python3_string = "python3 create_advanced_features.py " + training_file_path + " > temp_file"
		#print(advanced_python3_string)
		os.system(advanced_python3_string)
		
		advanced_cat_string = "cat temp_file >> swbdDAMSL.crfsuite.advanced.train"
		#print(advanced_cat_string)
		os.system(advanced_cat_string)
		
		advanced_echo_string = "echo \"\n\" >> swbdDAMSL.crfsuite.advanced.train"
		#print(advanced_echo_string)
		os.system(advanced_echo_string)
		
	#END for
#END for

for testing_subdirs, testing_dirs, testing_files in os.walk(TEST_DIR):
	for testing_file in sorted(testing_files):
		
		testing_file_path = os.path.join(TEST_DIR, testing_file)
		print(testing_file_path)
		
		# baseline test file
		baseline_python3_string = "python3 create_baseline_features.py " + testing_file_path + " > temp_file"
		#print(baseline_python3_string)
		os.system(baseline_python3_string)
		
		baseline_cat_string = "cat temp_file >> swbdDAMSL.crfsuite.baseline.test"
		#print(baseline_cat_string)
		os.system(baseline_cat_string)
		
		baseline_echo_string = "echo \"\n\" >> swbdDAMSL.crfsuite.baseline.test"
		#print(baseline_echo_string)
		os.system(baseline_echo_string)
		
		# advanced test file
		advanced_python3_string = "python3 create_advanced_features.py " + testing_file_path + " > temp_file"
		#print(advanced_python3_string)
		os.system(advanced_python3_string)
		
		advanced_cat_string = "cat temp_file >> swbdDAMSL.crfsuite.advanced.test"
		#print(advanced_cat_string)
		os.system(advanced_cat_string)
		
		advanced_echo_string = "echo \"\n\" >> swbdDAMSL.crfsuite.advanced.test"
		#print(advanced_echo_string)
		os.system(advanced_echo_string)
		
	#END for
#END for

for training_subdirs, training_dirs, training_files in os.walk(SELF_TRAIN_DIR):
	for training_file in sorted(training_files):
	
		training_file_path = os.path.join(SELF_TRAIN_DIR, training_file)
		print(training_file_path)
		
		# baseline training file
		baseline_python3_string = "python3 create_baseline_features.py " + training_file_path + " > temp_file"
		#print(baseline_python3_string)
		os.system(baseline_python3_string)
		
		baseline_cat_string = "cat temp_file >> crf.baseline.train"
		#print(baseline_cat_string)
		os.system(baseline_cat_string)
		
		baseline_echo_string = "echo \"\n\" >> crf.baseline.train"
		#print(baseline_echo_string)
		os.system(baseline_echo_string)
		
		# advanced training file
		advanced_python3_string = "python3 create_advanced_features.py " + training_file_path + " > temp_file"
		#print(advanced_python3_string)
		os.system(advanced_python3_string)
		
		advanced_cat_string = "cat temp_file >> crf.advanced.train"
		#print(advanced_cat_string)
		os.system(advanced_cat_string)
		
		advanced_echo_string = "echo \"\n\" >> crf.advanced.train"
		#print(advanced_echo_string)
		os.system(advanced_echo_string)
		
	#END for
#END for

for testing_subdirs, testing_dirs, testing_files in os.walk(SELF_TEST_DIR):
	for testing_file in sorted(testing_files):
		
		testing_file_path = os.path.join(SELF_TEST_DIR, testing_file)
		print(testing_file_path)
		
		# baseline test file
		baseline_python3_string = "python3 create_baseline_features.py " + testing_file_path + " > temp_file"
		#print(baseline_python3_string)
		os.system(baseline_python3_string)
		
		baseline_cat_string = "cat temp_file >> crf.baseline.test"
		#print(baseline_cat_string)
		os.system(baseline_cat_string)
		
		baseline_echo_string = "echo \"\n\" >> crf.baseline.test"
		#print(baseline_echo_string)
		os.system(baseline_echo_string)
		
		# advanced test file
		advanced_python3_string = "python3 create_advanced_features.py " + testing_file_path + " > temp_file"
		#print(advanced_python3_string)
		os.system(advanced_python3_string)
		
		advanced_cat_string = "cat temp_file >> crf.advanced.test"
		#print(advanced_cat_string)
		os.system(advanced_cat_string)
		
		advanced_echo_string = "echo \"\n\" >> crf.advanced.test"
		#print(advanced_echo_string)
		os.system(advanced_echo_string)
		
	#END for
#END for


os.system("rm temp_file")

baseline_train_file.close()
baseline_test_file.close()
advanced_train_file.close()
advanced_test_file.close()

baseline_self_train_file.close()
baseline_self_test_file.close()
advanced_self_train_file.close()
advanced_self_test_file.close()